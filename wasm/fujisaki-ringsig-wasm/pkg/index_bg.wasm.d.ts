/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function __wbg_opaqueptr_free(a: number): void;
export function init_wasm_bindings(): void;
export function wasm_init_tag(a: number, b: number): number;
export function wasm_tag_add_pubkey(a: number, b: number, c: number): number;
export function wasm_verify(a: number, b: number, c: number, d: number, e: number): number;
export function wasm_trace(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number): number;
export function wasm_free_tag(a: number): void;
export function generate_keypair(): number;
export function generate_keypair_from_bits(a: number): number;
export function get_pubkey(a: number, b: number): number;
export function get_privkey(a: number, b: number): number;
export function init_tag(a: number, b: number): number;
export function tag_add_pubkey(a: number, b: number): number;
export function sign(a: number, b: number, c: number, d: number, e: number): number;
export function verify(a: number, b: number, c: number, d: number, e: number): number;
export function do_trace(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number): number;
export function free_tag(a: number): void;
export function free_keypair(a: number): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
