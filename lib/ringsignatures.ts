let ringsig: any = null;
try {
  /* eslint-disable-next-line */
  ringsig = require('fujisaki-ringsig-wasm');
} catch (e) {
  console.log('Failed to load ring signature wasm:', e);
  console.log('Ring signatures are not supported in this environment');
}

/**
 * Ring signature tag consist of a issue (for example election ID)  and a list of
 * participating public keys.
 */
export class RingSignatureTag {
    tag: any;

    private constructor(tag: any) {
      this.tag = tag;
    }

    public static async make(
      issue: Uint8Array,
      participants: Uint8Array[],
    ): Promise<RingSignatureTag> {
      if (ringsig === null) {
        throw Error('Ring signature support not supported in this environment');
      }
      await ringsig.init_wasm_bindings();
      const tag = await ringsig.wasm_init_tag(issue);
      await Promise.all(participants.map((p) => ringsig.wasm_tag_add_pubkey(tag, p)));
      return new RingSignatureTag(tag);
    }

    public async free() {
      if (this.tag === null) {
        return;
      }
      await ringsig.wasm_free_tag(this.tag);
      this.tag = null;
    }
}

/**
 * Verify that signature is valid for tag & message.
 */
export function ringSignatureVerify(
  tag: RingSignatureTag,
  message: Uint8Array,
  signature: Uint8Array,
): Promise<boolean> {
  return ringsig.wasm_verify(message, tag.tag, signature);
}

export enum TraceResult {
    /// `Independent` indicates that the two given signatures were constructed
    /// with different private keys.
    Independent = 0,

    /// `Linked` indicates that the same private key was used to sign the same message under the
    /// same tag. This does not reveal which key performed the double-signature.
    Linked = 1,

    /// The same key was used to sign distinct messages under the same tag. `pubkey_out` reveals
    /// that pubkey.
    Revealed = 2,

    // We can also add error conditions
    InputErrorSig1 = 3,
    InputErrorSig2 = 4,
    InputErrorTag = 5,
}

export function ringSignatureTrace(
  tag: RingSignatureTag,
  message1: Uint8Array,
  signature1: Uint8Array,
  message2: Uint8Array,
  signature2: Uint8Array,

): Promise<TraceResult> {
  return ringsig.wasm_trace(
    message1,
    signature1,
    message2,
    signature2,
    tag.tag,
  );
}
