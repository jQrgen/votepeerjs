import {
  ringSignatureVerify,
  ringSignatureTrace,
  RingSignatureTag,
  TraceResult,
} from '../ringsignatures';

/**
 * For validating a vote payload. Verifies that it is correctly signed and not
 * a duplicate vote.
 */
export default class RingSignatureVoteValidator {
    private tag: RingSignatureTag;

    private constructor(tag: RingSignatureTag) {
      this.tag = tag;
    }

    static async make(
      electionID: Uint8Array,
      participants: Uint8Array[],
    ): Promise<RingSignatureVoteValidator> {
      return new RingSignatureVoteValidator(
        await RingSignatureTag.make(electionID, participants),
      );
    }

    async close(): Promise<void> {
      return this.tag.free();
    }

    async isAcceptable(
      vote: Uint8Array,
      signature: Uint8Array,
      alreadyAccepted: { vote: Uint8Array, signature: Uint8Array }[],
    ): Promise<[boolean, string | null]> {
      if (!await ringSignatureVerify(this.tag, vote, signature)) {
        return [false, 'Signature does not verify'];
      }

      let traces: TraceResult[] = [];
      try {
        /* eslint-disable-next-line max-len */
        traces = await Promise.all(alreadyAccepted.map((a) => ringSignatureTrace(this.tag, vote, signature, a.vote, a.signature)));
      } catch (e) {
        return [false, `Trace error: ${e}`];
      }

      for (const t of traces) {
        if (t !== TraceResult.Independent) {
          return [false, 'Trace not independent (double vote)'];
        }
      }
      return [true, null];
    }
}
