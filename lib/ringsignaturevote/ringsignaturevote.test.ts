import RingSignatureVote from './ringsignaturevote';

const election = new RingSignatureVote(
  // Salt is *not* hex encoded.
  Buffer.from('51f20e3f84998a41bef6b768394726d0403c935c046f3d53e494f1553b442ea6'),
  'Anon',
  700818,
  709755,
  ['A', 'B'],
  [
    // bitcoincash:qrq08q63ymlxn95f9ptyq65jjwekzsdl2gp8rde6j0
    Uint8Array.from(Buffer.from(
      'a03d120c227b9c8cbe9fa3775b377358fcd8a756c45fedfa5c50bcb0cdb45864', 'hex',
    )),
    // bitcoincash:qqj3uzc9f8uzqgrdjhj609zqcdaj2h228uf9fae79k
    Uint8Array.from(Buffer.from(
      '6edcf341f28bba48e7701c93a60917f640b98b180110d09c5fa4ef24c5affc55', 'hex',
    )),
  ],
);

describe('RingSignatureVote', () => {
  test('getElectionID', async () => {
    const electionID = Buffer.from(await election.getElectionID()).toString('hex');
    expect(electionID).toBe('d231b87923ed25b9a536a5d0f3ed4b18c038406d');
  });

  test('getNotificationAddress', async () => {
    const notificationAddress = await election.getNotificationAddress();
    expect(notificationAddress).toBe('bitcoincash:prvsjxtfygelawad7t87t2rj8rr5u87vuu5sek8jp6');
  });

  test('getVoteOptionHashed', async () => {
    const hashedOption = await election.getVoteOptionHashed(0);
    expect(Buffer.from(hashedOption).toString('hex')).toBe('71531d7c9023306ccfddc307096388f5ce78fdfd');
  });

  test('isValidVoteOptionHash', async () => {
    const hashedOption = await election.getVoteOptionHashed(0);
    expect(await election.isValidVoteOptionHash(hashedOption)).toBe(true);
    expect(await election.isValidVoteOptionHash(Buffer.from('baadf00d', 'hex'))).toBe(false);
  });

  test('getOptionFromHash', async () => {
    const option = election.voteOptions[0];
    const hashedOption = await election.getVoteOptionHashed(0);
    expect(await election.getOptionFromHash(hashedOption)).toBe(option);
  });
});
