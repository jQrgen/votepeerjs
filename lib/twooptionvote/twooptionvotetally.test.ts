import { tallyTwoOptionVotes } from './twooptionvotetally';
import { getExampleElection } from './testutil';
import Blockchain from '../blockchain';
import { TwoOptionVote } from './twooptionvote';
import { hash160Salted } from '../crypto';
import { hashToAddress } from '../transaction';
import { BLANK_VOTE } from './votedef';

/* eslint-disable max-len */

test('tallyTwoOptionVotes', async () => {
  jest.setTimeout(30000);
  const election = await getExampleElection();

  const chain = new Blockchain();
  await chain.connect();

  const optionAHash = Buffer.from(await hash160Salted(election.salt, Buffer.from(election.optionA)));
  const optionBHash = Buffer.from(await hash160Salted(election.salt, Buffer.from(election.optionB)));
  const blank = Buffer.from(BLANK_VOTE);

  const tally = await tallyTwoOptionVotes(chain, election, true, true);
  const optionAVotes = tally.accepted.filter((b) => optionAHash.equals(b.vote));
  const optionBVotes = tally.accepted.filter((b) => optionBHash.equals(b.vote));
  const blankVotes = tally.accepted.filter((b) => blank.equals(b.vote));

  const noVote = election.votersPKH.length - (
    optionAVotes.length
          + optionBVotes.length
          + blankVotes.length
          + tally.rejected.length);

  expect(optionAVotes.length).toBe(1);
  expect(optionBVotes.length).toBe(1);
  expect(blankVotes.length).toBe(2);
  expect(tally.rejected.length).toBe(1);
  expect(noVote).toBe(1);

  expect(optionAVotes[0].participant).toBe(hashToAddress(election.votersPKH[0], 'p2pkh'));
  expect(optionBVotes[0].participant).toBe(hashToAddress(election.votersPKH[1], 'p2pkh'));
  expect(blankVotes[0].participant).toBe(hashToAddress(election.votersPKH[2], 'p2pkh'));
  expect(blankVotes[1].participant).toBe(hashToAddress(election.votersPKH[3], 'p2pkh'));
  expect(tally.rejected[0].participant).toBe(hashToAddress(election.votersPKH[4], 'p2pkh'));

  chain.disconnect();
});

/**
 * Test that duplicate votes in later blocks are ignored.
 *
 * In this example, one of the voters cast the same vote twice. However, one of
 * them confirmed before the other, so the vote does not get invalidated.
 */
test('ignore_later_votes', async () => {
  const votersPKH = [
    Buffer.from('4680a9baaa0fcd416742ccdbf6b302554acdb898', 'hex'),
    Buffer.from('d9ba804bbdebd5dcf41c0a49aacc135552e78e11', 'hex'),
  ];
  const salt = Buffer.from('4f56c48af8c73f48a7b9d91dce32303b4b6fe378', 'hex');

  const election: TwoOptionVote = {
    network: 'mainnet',
    salt,
    description: 'Pizza for lunch?',
    optionA: 'Yes',
    optionB: 'No',
    endHeight: 1_000_000,
    votersPKH,
  };
  const optionAHash = Buffer.from(await hash160Salted(election.salt, Buffer.from(election.optionA)));
  const optionBHash = Buffer.from(await hash160Salted(election.salt, Buffer.from(election.optionA)));
  const electrum = new Blockchain();
  await electrum.connect();
  const tally = await tallyTwoOptionVotes(electrum, election, true);
  const optionAVotes = tally.accepted.filter((b) => optionAHash.equals(b.vote));
  const optionBVotes = tally.accepted.filter((b) => optionBHash.equals(b.vote));
  expect(optionAVotes.length).toBe(1);
  expect(optionBVotes.length).toBe(1);
  electrum.disconnect();
});
