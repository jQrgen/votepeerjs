/* eslint max-classes-per-file: ["error", 2] */
/* eslint no-empty-function: "off" */
import { calculateProposalID, deriveContractAddress } from './primitives';
import { parseVoteFromTx, fetchVoteTx } from './voteverify';
import Blockchain from '../blockchain';
import { TwoOptionVote } from './twooptionvote';
import NetworkTransaction from '../networktransaction';
import { hash160Salted } from '../crypto';
import {
  AcceptedBallotTransaction,
  RejectedBallotTransaction,
  BallotResult,
} from '../tally';
import { hashToAddress } from '../transaction';
import NoVoteError from '../error/novoteerror';
import InvalidVoteError from '../error/invalidvoteerror';

/**
 * Queries the blockchain for a transactions for a past or ongoing vote.
 *
 * @param chain - Electrum connection
 * @param election - A two-option-vote election
 * @param includeUnconfirmed - Include unconfirmed ballots (if applicable)
 * @param includeRejected - Include rejected transactions
 */
export async function tallyTwoOptionVotes(
  chain: Blockchain,
  election: TwoOptionVote,
  includeUnconfirmed: boolean,
  includeRejected: boolean = true,
): Promise<BallotResult> {
  const [proposalID, optionAHash, optionBHash] = await Promise.all([
    calculateProposalID(election),
    hash160Salted(election.salt, Buffer.from(election.optionA)),
    hash160Salted(election.salt, Buffer.from(election.optionB))]);

  const contractAddresses: string[] = await Promise.all(
    /* eslint-disable-next-line max-len */
    election.votersPKH.map((pkh: Uint8Array) => deriveContractAddress(proposalID, optionAHash, optionBHash, pkh)),
  );

  const voteTxs: (NetworkTransaction | null | string)[] = await Promise.all(
    contractAddresses.map(async (a) => {
      try {
        return await fetchVoteTx(
          chain, a,
          election.endHeight,
          includeUnconfirmed,
        );
      } catch (e) {
        if (e instanceof NoVoteError) {
          return null;
        }
        if (e instanceof InvalidVoteError) {
          // Rejected vote
          return e.toString();
        }
        // Network error? Best to give up tallying.
        throw e;
      }
    }),
  );

  const votes: ([Uint8Array, Uint8Array] | null)[] = voteTxs.map((tx, i) => {
    if (tx === null) {
      return null;
    }
    if (typeof tx === 'string') {
      // Rejected, don't attempt to parse it.
      return null;
    }
    try {
      return parseVoteFromTx(
        tx.getTransaction(), proposalID,
        optionAHash, optionBHash,
        election.votersPKH[i],
      );
    } catch (e) {
      console.debug(`Error parsing vote: ${e}`);
      return null;
    }
  });

  const participants: string[] = election.votersPKH.map((pkh: Uint8Array) => hashToAddress(pkh, 'p2pkh'));
  const accepted: AcceptedBallotTransaction[] = [];
  const rejected: RejectedBallotTransaction[] = [];

  /* eslint-disable-next-line array-callback-return */
  votes.map((vote, i) => {
    if (vote === null && voteTxs[i] === null) {
      // Participant `i` has not voted.
      return;
    }
    if (typeof voteTxs[i] === 'string') {
      /* eslint-disable-next-line no-unused-expressions */
      includeRejected && rejected.push({
        transaction: voteTxs[i] as NetworkTransaction,
        reason: voteTxs[i] as string,
        participant: participants[i],
      });
      return;
    }
    if (vote === null) {
      // If vote is not a string and tx is not null,
      // then we should have successfully parsed a vote at this point.
      throw new Error('Should have parsed vote from tx');
    }
    accepted.push({
      transaction: voteTxs[i] as NetworkTransaction,
      vote: vote[0],
      signature: vote[1],
      participant: participants[i],
    });
  });

  return {
    accepted,
    rejected,
  };
}
