/* eslint class-methods-use-this: "off" */
/* eslint no-param-reassign: "off" */
import {
  bigIntToBinUint64LE, cashAddressToLockingBytecode, encodeDataPush,
  encodeTransaction, generateTransaction,
} from '@bitauth/libauth';
import { getTxID, hashToAddress, signTransactionInput } from '../transaction';
import {
  calculateProposalID, createVoteMessage,
  twoOptionContractRedeemscript, deriveContractAddress, signVoteMessage,
} from './primitives';
import {
  getPublicKeyHash, derivePublicKey, hash160Salted,
} from '../crypto';
import { BLANK_VOTE, DEFAULT_CAST_TX_FEE } from './votedef';
import Blockchain from '../blockchain';
import { DEFAULT_DUST_THRESHOLD } from '../networkconstants';
import { getOrFundUtxo, waitForBalance } from '../utiladdress';

export interface TwoOptionVote {
    readonly network: 'mainnet' | 'testnet';
    readonly salt: Uint8Array,
    readonly description: string,
    readonly optionA: string,
    readonly optionB: string,
    readonly votersPKH: Uint8Array[],
    readonly endHeight: number,
}

export class TwoOptionVoteContract {
    private election: TwoOptionVote;

    readonly voterPublicKey: Uint8Array;

    private voterPrivateKey: Uint8Array;

    private cachedElectionID: Uint8Array | null = null;

    private constructor(
      election: TwoOptionVote,
      voterPrivateKey: Uint8Array,
      voterPublicKey: Uint8Array,
    ) {
      this.election = election;
      this.voterPrivateKey = voterPrivateKey;
      this.voterPublicKey = voterPublicKey;
    }

    public async electionID(): Promise<Uint8Array> {
      if (this.cachedElectionID !== null) {
        return this.cachedElectionID;
      }
      this.cachedElectionID = await calculateProposalID(this.election);
      return this.cachedElectionID;
    }

    /**
      * Create a contract instance for a participating voter.
      *
      * @param election - The election voter is participating in.
      * @param voterPrivateKey - Private key used for signing the vote and the transaction itself.
      */
    public static async make(election: TwoOptionVote,
      voterPrivateKey: Uint8Array): Promise<TwoOptionVoteContract> {
      const voterPublicKey = await derivePublicKey(voterPrivateKey);
      return new TwoOptionVoteContract(election, voterPrivateKey, voterPublicKey);
    }

    /**
      * The bitcoin cash address for this contract.
      */
    public async getContractAddress(): Promise<string> {
      return deriveContractAddress(
        await this.electionID(),
        await this.optionAHash(),
        await this.optionBHash(),
        await this.voterPKH(),
      );
    }

    /**
      * Wait for contract to contain at minimum this balance.
      *
      * @param minimumBalance - Minimum balance before returning.
      * @param waitForBalance - Called at intervals while waiting on balance.
      */
    public async waitForBalance(
      blockchain: Blockchain,
      minimumBalance: number,
      waitCallback: (balance: number) => void,
    ): Promise<number> {
      return waitForBalance(
        blockchain,
        await this.getContractAddress(),
        minimumBalance,
        waitCallback,
      );
    }

    /**
      * Creates a messaged + signature for message with signed vote.
      *
      * @param privatekey - Private key of the participant owning this contract.
      * @param option - The option to vote on.
      */
    public async signVoteMessage(privatekey: Uint8Array,
      optionHash: Uint8Array): Promise<[Uint8Array, Uint8Array]> {
      const id = await calculateProposalID(this.election);
      const message = createVoteMessage(id, optionHash);
      const signature = await signVoteMessage(privatekey, message);
      return [message, signature];
    }

    /**
      * The salted hash of vote option A.
      */
    public async optionAHash(): Promise<Uint8Array> {
      return hash160Salted(this.election.salt, Buffer.from(this.election.optionA));
    }

    /**
      * The salted hash of vote option B.
      */
    public async optionBHash(): Promise<Uint8Array> {
      return hash160Salted(this.election.salt, Buffer.from(this.election.optionB));
    }

    /**
      * The hash for blank vote.
      */
    public optionBlank(): Uint8Array {
      return BLANK_VOTE;
    }

    /**
      * Get P2PKH address of voter (not the contract address!)
      */
    public async getVoterAddress() {
      return hashToAddress(await this.voterPKH(), 'p2pkh');
    }

    async unlockingBytecode(
      txSignature: Uint8Array,
      voteMessage: Uint8Array,
      voteSignature: Uint8Array,
    ): Promise<Uint8Array> {
      return Buffer.concat([
        encodeDataPush(voteSignature),
        encodeDataPush(voteMessage),
        encodeDataPush(txSignature),
        encodeDataPush(this.voterPublicKey),
        encodeDataPush(await this.getRedeemScript()),
      ]);
    }

    /**
      * Create and submit a transaction that performs a vote.
      *
      * @param privatekey - Private key of owner of the contract.
      * @param optionHash - The option to vote for.
      * @param changeAddress - Where to send contract coins to. Defaults to users P2PKH address.
      * @returns Transaction ID
      */
    public async castVote(
      electrum: Blockchain,
      optionHash: Uint8Array,
    ): Promise<string> {
      const fundingRequired = DEFAULT_CAST_TX_FEE + DEFAULT_DUST_THRESHOLD; // satoshis
      const castUtxo = await getOrFundUtxo(
        electrum,
        await this.getContractAddress(),
        fundingRequired,
        this.voterPrivateKey,
      );

      const inputs = [{
        outpointIndex: castUtxo.outpointIndex,
        outpointTransactionHash: castUtxo.outpointTransactionHash,
        unlockingBytecode: Buffer.alloc(0),
        sequenceNumber: 0,
      }];

      // We need an output for it to be a valid transaction. Doesn't matter what it is.
      // Let's just dust ourselves.
      const outputAddress = await this.getVoterAddress();
      const outputLockingScript: Uint8Array = (cashAddressToLockingBytecode(outputAddress) as any)
        .bytecode;
      const voteCastTx = generateTransaction({
        inputs,
        outputs: [{
          lockingBytecode: outputLockingScript,
          satoshis: bigIntToBinUint64LE(BigInt(castUtxo.satoshis - DEFAULT_CAST_TX_FEE)),
        }],
        locktime: 0,
        version: 2,
      });

      if (!voteCastTx.success) {
        throw voteCastTx.errors;
      }

      const txSig = await signTransactionInput(
        voteCastTx.transaction,
        castUtxo.satoshis,
        castUtxo.outpointIndex,
        await this.getRedeemScript(),
        this.voterPrivateKey,
      );

      const [vote, voteSig] = await this.signVoteMessage(
        this.voterPrivateKey, optionHash,
      );

      voteCastTx.transaction.inputs[0].unlockingBytecode = await this.unlockingBytecode(
        txSig, vote, voteSig,
      );

      console.log(Buffer.from(encodeTransaction(voteCastTx.transaction)).toString('hex'));
      const voteCastTxID = await getTxID(voteCastTx.transaction);
      const broadcastTxID = await electrum.broadcast(encodeTransaction(voteCastTx.transaction));
      if (broadcastTxID !== voteCastTxID) {
        console.warn(`Expected electrum server to return txid ${voteCastTxID}, got ${broadcastTxID}`);
      }
      return voteCastTxID;
    }

    /**
      * Get the public key hash of the voter this contract instance belongs to.
      */
    public async voterPKH(): Promise<Uint8Array> {
      return getPublicKeyHash(this.voterPublicKey);
    }

    public async getRedeemScript(): Promise<Buffer> {
      return twoOptionContractRedeemscript(
        await this.electionID(),
        await this.optionAHash(),
        await this.optionBHash(),
        await this.voterPKH(),
      );
    }
}
