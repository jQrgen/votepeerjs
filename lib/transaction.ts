import {
  CashAddressType, CashAddressNetworkPrefix, decodeCashAddress, Transaction,
  encodeCashAddress, encodeTransaction, createTransactionContextCommon, bigIntToBinUint64LE,
  generateSigningSerializationBCH,
  SigningSerializationFlag,

} from '@bitauth/libauth';
import { assert } from 'console';
import {
  sha256d, sha256Hasher, signHashWithSchnorr,
} from './crypto';
import { reverseBuffer } from './utillist';

/**
 * Generate bitcoin cash address from PKH or SH.
 * @param hash - Public key hash or script hash
 * @param addressType - 'p2pkh' or 'p2sh'
 */
export function hashToAddress(hash: Uint8Array, addressType: string): string {
  if (addressType !== 'p2sh' && addressType !== 'p2pkh') {
    throw new Error('Unsupported address type');
  }

  let type = CashAddressType.P2PKH;
  if (addressType === 'p2sh') {
    type = CashAddressType.P2SH;
  }
  const prefix = CashAddressNetworkPrefix.mainnet;
  return encodeCashAddress(prefix, type, hash);
}

/**
 * Convert a bitcoin cash address to an output script.
 *
 * @param address - cashaddr
 * @returns output script
 */
export function toOutputScript(address: string): Buffer {
  const decoded: any = decodeCashAddress(address);
  if (decoded.type === CashAddressType.P2PKH) {
    return Buffer.concat([
      Buffer.from([0x76]), // OP_DUP
      Buffer.from([0xa9]), // OP_HASH160
      Buffer.from([20]), // OP push 20 bytes
      decoded.hash,
      Buffer.from([0x88]), // OP_EQUALVERIFY
      Buffer.from([0xac]), // OP_CHECKSIG
    ]);
  }
  if (decoded.type === CashAddressType.P2SH) {
    return Buffer.concat([
      Buffer.from([0xa9]), // OP_HASH160
      Buffer.from([20]), // OP push 20 bytes
      decoded.hash,
      Buffer.from([0x87]), // OP_EQUAL
    ]);
  }
  throw Error('unknown cashaddr');
}

/**
 * Calculate the transaction ID of a transaction
 * @param tx - Bitcoin Cash transaction
 * @returns Transaction ID as hex string
 */
export async function getTxID(tx: Transaction): Promise<string> {
  const encoded = encodeTransaction(tx);
  const hashed = await sha256d(Buffer.from(encoded));
  return Buffer.from(reverseBuffer(hashed)).toString('hex');
}

/**
 * If size could be a Bitcoin Cash ECDSA or Schnorr signature.
 */
export function isPlausibleSignatureSize(signatureSize: number) {
  return signatureSize >= 64 && signatureSize <= 73;
}

/**
 * Decode a hash from a Bitcoin Cash address.
 *
 * @param address - Bitcoin Cash address
 * @returns Decoded hash from address
 */
export function hashFromAddress(address: string): Uint8Array {
  const decoded: any = decodeCashAddress(address);
  if (decoded.hash === undefined) {
    throw Error('Failed to decode address');
  }
  return decoded.hash;
}

/**
 * Hash a transaction for use with OP_CHECKSIG
 *
 * @param transaction - Transaction to hash
 * @param inputSatoshis - Satoshis in input we're hashing for
 * @param inputIndex - Index of input we're hashing for
 * @param coveredBytecode - The redeem script of the input we're spending
 * @returns sighash
 */
export async function createTransactionSighash(
  transaction: Transaction,
  inputSatoshis: number,
  inputIndex: number,
  coveredBytecode: Uint8Array,
  hashType: Uint8Array,
): Promise<Uint8Array> {
  assert(hashType.length === 1, 'expected hashType to be one byte');
  assert(hashType[0] === 0x41, 'expected SIGHASH_ALL | FORK_ID');
  const state = createTransactionContextCommon({
    inputIndex,
    sourceOutput: { satoshis: bigIntToBinUint64LE(BigInt(inputSatoshis)) },
    spendingTransaction: transaction,
  });

  const sighashPreimage = generateSigningSerializationBCH({
    correspondingOutput: undefined,
    coveredBytecode,
    locktime: transaction.locktime,
    outpointIndex: transaction.inputs[inputIndex].outpointIndex,
    outpointTransactionHash: transaction.inputs[inputIndex].outpointTransactionHash,
    outputValue: bigIntToBinUint64LE(BigInt(inputSatoshis)),
    sequenceNumber: transaction.inputs[inputIndex].sequenceNumber,
    sha256: await sha256Hasher(),
    signingSerializationType: hashType,
    transactionOutpoints: state.transactionOutpoints,
    transactionOutputs: state.transactionOutputs,
    transactionSequenceNumbers: state.transactionSequenceNumbers,
    version: transaction.version,
  });

  return sha256d(sighashPreimage);
}

export async function signTransactionInput(
  tx: Transaction,
  inputSatoshis: number,
  inputIndex: number,
  coveredBytecode: Uint8Array,
  privateKey: Uint8Array,
): Promise<Uint8Array> {
  /* eslint-disable-next-line */
  const hashType = Uint8Array.from([SigningSerializationFlag.allOutputs | SigningSerializationFlag.forkId]);
  const sighash = await createTransactionSighash(
    tx, inputSatoshis, inputIndex, coveredBytecode, hashType,
  );

  return Buffer.concat([await signHashWithSchnorr(privateKey, sighash), hashType]);
}
