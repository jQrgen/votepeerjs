/**
 * Encode a number as unsigned 32 bit big endian.
 */
export function numberToUint32BE(n: number): Buffer {
  const b = Buffer.alloc(4);
  b.writeUInt32BE(n, 0);
  return b;
}
