export async function allIgnoringErrors<T>(
  promises: Promise<T>[],
  debugLog: boolean = false,
): Promise<(T | null
)[]> {
  return Promise.all(promises.map(async (p) => {
    try {
      return await p;
    } catch (e) {
      if (debugLog) {
        console.debug('Ignoring error ', e);
      }
      return null;
    }
  }));
}

/**
 * Reverse a buffer.
 * @param buffer - Source buffer
 * @returns New buffer that is the reverse of source buffer.
 */
export function reverseBuffer(buffer: Uint8Array): Uint8Array {
  const reversed = new Uint8Array(buffer.length);

  let i = 0;
  let j = buffer.length;

  while (j > 0) {
    reversed[i++] = buffer[--j];
  }

  return reversed;
}

/**
 * Used for sorting arrays of Uint8Array (array.sort(byteArrayComparator)).
 */
export function byteArrayComparator(a: any, b: any): number {
  return Buffer.from(a).compare(b);
}
