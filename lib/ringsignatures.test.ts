import {
  RingSignatureTag, ringSignatureVerify, ringSignatureTrace, TraceResult,
} from './ringsignatures';

function fromHex(hex: string): Uint8Array {
  return Uint8Array.from(Buffer.from(hex, 'hex'));
}

class TestData {
  static issue = fromHex('736F6D65696400');

  static pubKeys = [
    fromHex('B81BFF10718421BED8FB1B58D568F47A521F602A4408060D3AEA7BD94960012E'),
    fromHex('E817EED1126862798A3E7A4D302DF2C3593BA5B8D77BFC691CA7AE4F212AEC1E'),
    fromHex('FE92868A45EF1FAD169D442F8FC46466F0E8451FEAEE0195C5C1FFC795004330'),
  ];

  static message1 = fromHex('6D657373616765203100');

  static message2 = fromHex('6D657373616765203200');

  static signature1 = fromHex('86CE9180842FD29688750B5B3410A94E2AAA1A7B0E8E7CCC3A32BD5B7885372A0300000000000000D0EEF4E4B714BCA07AAA3DEE525A0AF26ABE59D1B01E830CD266285BD245580D73B4DAB904AE96D22A41EE1BD74EB577F2B4F026B99C6CE1632AB13F19CF980CD3F8EE3E720AA57A2E3140FEA9391601C175D62768FE0751633CAB88156BDE0D0300000000000000EFE3FFCA8D016AE0159D487741E15B72ABAEBEF1756A7C14DEBC4D4B3CD3D700D04108022E9D5094345CF403896427B7998FCBBCDF6AE1F1F7B3E2E797C35107990B69575FAB0DAE944586895DA2594F4C668B0B1E9A73C29B040797E7E57903');

  static signature2 = fromHex('8A81D0369B17F1F35EEEDD3C0BD91CC165A596DF5170D289DE15987EA4CF2E3D03000000000000006F372A2A571A00F684635DE9785D442D17BDC3ED7458D5B6E19F5E297C01D405EFD36F9A532A09590C999C101AC08AF1D1C7EB1D84BD5CCCD95C3B7BF6322004A8F4B15C11153A3B383AB4BDA54B4CDE2B490FDAA4F24063E0AEDF4628B76C0203000000000000002D574DB7C7916034C5AB9728E5CFFA2A071315E171708121AA623A9AFA2FBA05E25626ABB57E68CAD98EC238B368D21A3635171F1BC26614C9A73D586A501207E922AD85C7B75B67445970C69BB5702DACC01843373418F7FB2D9F11202CBE06');
}

describe('ring signatures', () => {
  test('createTag', async () => {
    const r = await RingSignatureTag.make(TestData.issue, TestData.pubKeys);
    expect(r.tag).not.toEqual(null);
    await r.free();
  });

  test('verify', async () => {
    const tag = await RingSignatureTag.make(TestData.issue, TestData.pubKeys);
    expect(await ringSignatureVerify(tag,
      TestData.message1,
      TestData.signature1)).toBe(true);
    expect(await ringSignatureVerify(tag,
      TestData.message2,
      TestData.signature2)).toBe(true);
    expect(await ringSignatureVerify(tag,
      TestData.message1,
      TestData.signature2)).toBe(false);
    expect(await ringSignatureVerify(tag,
      TestData.message2,
      TestData.signature1)).toBe(false);
    await tag.free();
  });

  test('trace', async () => {
    const tag = await RingSignatureTag.make(TestData.issue, TestData.pubKeys);
    const trace = await ringSignatureTrace(
      tag,
      TestData.message1,
      TestData.signature1,
      TestData.message2,
      TestData.signature2,
    );
    expect(trace).toBe(TraceResult.Revealed);
    await tag.free();
  });
});
