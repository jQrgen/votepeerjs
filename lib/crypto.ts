import {
  generatePrivateKey as genPrivKey, Secp256k1, instantiateSecp256k1, instantiateSha256,
  instantiateRipemd160, Sha256, Ripemd160,
} from '@bitauth/libauth';
import assert from 'assert';
import crypto from 'crypto';

// Instantiate wasm libraries.
const sha256Promise = instantiateSha256();
const ripemd160Promise = instantiateRipemd160();
const secp256k1Promise = instantiateSecp256k1();

export async function sha256Hasher(): Promise<Sha256> {
  return sha256Promise;
}

export async function ripemd160Hasher(): Promise<Ripemd160> {
  return ripemd160Promise;
}

export async function secp256k1(): Promise<Secp256k1> {
  return secp256k1Promise;
}

/**
 * Generate 20 bytes of random data
 */
export function createSalt(): Uint8Array {
  return crypto.randomBytes(20);
}

/**
 * sha256(buffer)
 * @param buffer - Data to be hashed
 */
export async function sha256(buffer: Uint8Array): Promise<Uint8Array> {
  const libSha256 = await sha256Hasher();
  const state1 = libSha256.init();
  const state2 = libSha256.update(state1, buffer);
  return libSha256.final(state2);
}

/**
 * Result of RIPEMD160(SHA256(buffer))
 * @param buffer - Data to be hashed
 */
export async function hash160(buffer: Uint8Array): Promise<Uint8Array> {
  const sha = await sha256(buffer);
  const ripemd160 = await ripemd160Hasher();
  const state1 = ripemd160.init();
  const state2 = ripemd160.update(state1, sha);
  return ripemd160.final(state2);
}

/**
 * Double sha256 of buffer, sha256(sha256(buffer))
 * @param buffer - Data to be hashed
 */
export async function sha256d(buffer: Uint8Array): Promise<Uint8Array> {
  const roundOne = await sha256(buffer);
  const roundTwo = await sha256(roundOne);
  return roundTwo;
}

/**
 * Concatenates salt with buffer and runs hash160 on it.
 * See {@link hash160}
 * @param buffer - Data to be hashed
 */
export async function hash160Salted(salt: Uint8Array, buffer: Uint8Array): Promise<Uint8Array> {
  return hash160(Buffer.concat([Buffer.from(salt), Buffer.from(buffer)]));
}

/**
 * Generates a random private key.
 */
export function generatePrivateKey() : Uint8Array {
  return genPrivKey(() => crypto.randomBytes(32));
}

/**
 * Derives the public key of a private key.
 * @param privateKey - Bitcoin private key
 */
export async function derivePublicKey(privateKey: Uint8Array): Promise<Uint8Array> {
  const libsecp256k1 = await secp256k1();
  return libsecp256k1.derivePublicKeyCompressed(privateKey);
}

/**
 * Calculates the PKH (public key hash) of a public key. Used
 * in P2PKH transactions.
 */
export async function getPublicKeyHash(pubkey: Uint8Array): Promise<Uint8Array> {
  return hash160(pubkey);
}

/**
 * Signs the sha256 of a message using Schnorr algorithm.
 * @param privateKey - The private key to sign with.
 * @param message - The message to sign hash of.
 */
export async function signHashOfMessageWithSchnorr(
  privateKey: Uint8Array, message: Uint8Array,
): Promise<Uint8Array> {
  const libsecp256k1 = await secp256k1();
  const messageHash = await sha256(message);
  return libsecp256k1.signMessageHashSchnorr(privateKey, messageHash);
}

export async function signHashWithSchnorr(
  privateKey: Uint8Array, hash: Uint8Array,
): Promise<Uint8Array> {
  assert(privateKey.length === 32, 'expected private key to be 64 bytes');
  assert(hash.length === 32, 'expected hash to be 32 bytes');
  return (await secp256k1()).signMessageHashSchnorr(privateKey, hash);
}
