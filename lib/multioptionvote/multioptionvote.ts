import {
  CashAddressType, encodeCashAddress, encodeDataPush, OpcodesBCH, OpcodesCommon, Transaction,
} from '@bitauth/libauth';
import assert from 'assert';
import { hash160, sha256 } from '../crypto';
import { byteArrayComparator } from '../utillist';
import { numberToUint32BE } from '../utilserialize';

const NULL_HASH = Buffer.alloc(32, 0x0);

export class MultiOptionVote {
    readonly salt: Uint8Array

    readonly description: string

    readonly beginHeight: number

    readonly endHeight: number

    readonly voteOptions: Array<string>

    readonly voteOptionsHashed: Promise<Array<Uint8Array>>

    /**
     * List of participants (public key hashes)
     */
    readonly participants: Array<Uint8Array>

    constructor(
      salt: Uint8Array,
      description: string,
      beginHeight: number,
      endHeight: number,
      voteOptions: Array<string>,
      participants: Array<Uint8Array>,
    ) {
      this.voteOptionsHashed = Promise.all(
        voteOptions.map((o) => hash160(Buffer.concat([salt, Buffer.from(o)]))),
      );
      participants.sort(byteArrayComparator);

      this.salt = salt;
      this.description = description;
      this.beginHeight = beginHeight;
      this.endHeight = endHeight;
      this.voteOptions = voteOptions;
      this.participants = participants;
    }

    /**
     * A participant merkle tree looks like this:
     * ```
     *                root
     *              /     \
     *             N      N
     *           / |      | \
     *         H   H      H  Ø
     *         |   |      |
     *        P1  P2     P3
     * ```
     */
    async participantsMerkleRootHash(): Promise<Uint8Array> {
      if (this.participants.length === 0) {
        throw Error('Participants cannot be empty');
      }

      // Wrap leafs
      let queue: Uint8Array[] = await Promise.all(this.participants.map((p) => sha256(p)));

      // Special case: If there is only 1 participant leaf, we need to pair
      // it with a Ø-node.
      //
      // Without this, the node would be the merkle root and we would
      // not be able to generate "proof of number of participants".
      if (queue.length === 1) {
        queue.push(NULL_HASH);
      }

      const createParents = () => {
        assert(queue.length % 2 === 0);
        const parents: Array<Promise<Uint8Array>> = [];

        while (queue.length > 0) {
          const left = queue.shift() as Uint8Array;
          const right = queue.shift() as Uint8Array;
          parents.push(sha256(Buffer.concat([left, right])));
        }
        return Promise.all(parents);
      };

      while (queue.length !== 1) {
        if (queue.length % 2 !== 0) {
          // Push a NULL node to balance all participants to same level.
          queue.push(NULL_HASH);
        }
        queue = await createParents();
      }
      return queue.shift() as Uint8Array;
    }

    private electionIDCached: Uint8Array | null = null;

    /**
     * The Election ID consist of a hash of all the data belonging to a vote.
     */
    async electionID(): Promise<Uint8Array> {
      if (this.electionIDCached !== null) {
        return this.electionIDCached as Uint8Array;
      }
      const voteOptionsSorted = [...await this.voteOptionsHashed]; // clone
      voteOptionsSorted.sort(byteArrayComparator);

      this.electionIDCached = await hash160(Buffer.concat([
        this.salt,
        Buffer.from(this.description),
        numberToUint32BE(this.beginHeight),
        numberToUint32BE(this.endHeight),
        Buffer.concat(voteOptionsSorted),
        await this.participantsMerkleRootHash(),
      ]));
      return this.electionIDCached;
    }

    async voterContractAddress(pubkeyHash: Uint8Array): Promise<string> {
      const hash = await hash160(await this.redeemScript(pubkeyHash));
      return encodeCashAddress('bitcoincash', CashAddressType.P2SH, hash);
    }

    /**
     * The multi-option-vote Script.
     */
    async redeemScript(voterPKH: Uint8Array): Promise<Uint8Array> {
      const electionID = await this.electionID();
      return Buffer.concat([
        Buffer.alloc(1, OpcodesCommon.OP_OVER),
        Buffer.alloc(1, OpcodesCommon.OP_CHECKSIGVERIFY),
        Buffer.alloc(1, OpcodesCommon.OP_DUP),
        Buffer.alloc(1, OpcodesCommon.OP_HASH160),
        encodeDataPush(voterPKH),
        Buffer.alloc(1, OpcodesCommon.OP_EQUALVERIFY),
        encodeDataPush(Buffer.alloc(1, 0x02)),
        Buffer.alloc(1, OpcodesCommon.OP_PICK),
        encodeDataPush(electionID),
        Buffer.alloc(1, OpcodesCommon.OP_EQUALVERIFY),
        Buffer.alloc(1, OpcodesCommon.OP_ROT),
        Buffer.alloc(1, OpcodesCommon.OP_ROT),
        Buffer.alloc(1, OpcodesCommon.OP_CAT),
        Buffer.alloc(1, OpcodesCommon.OP_SWAP),
        Buffer.alloc(1, OpcodesBCH.OP_CHECKDATASIGVERIFY),
        Buffer.alloc(1, OpcodesCommon.OP_DEPTH),
        Buffer.alloc(1, OpcodesCommon.OP_NOT),
      ]);
    }

    async getOptionFromHash(hash: Uint8Array): Promise<string> {
      const optionsHashed = (await this.voteOptionsHashed)
        .map((x) => Buffer.from(x));

      for (let i = 0; i < optionsHashed.length; ++i) {
        if (optionsHashed[i].equals(hash)) {
          return this.voteOptions[i];
        }
      }
      throw Error('Invalid vote hash for election');
    }

    /**
         * Parse the vote from a multi-option-vote transaction.
         *
         * Does not validate if it's a vote contract, nor does it validate the signature!
         */
    static parseVote(
      tx: Transaction,
      validHashedVoteOptions: Uint8Array[],
      inputIndex: number = 0,
    ): [Uint8Array, Uint8Array] {
      if (tx.inputs.length < inputIndex + 1) {
        throw Error('tx.inputs.size < inputIndex + 1');
      }
      const script = tx.inputs[inputIndex].unlockingBytecode;
      let pos = 0;
      if (script.length === 0) throw Error('tx.inputs[0].unlockingBytecode is an empty array');

      const voteSignatureSize = script[pos++];
      if (voteSignatureSize !== 64) {
        throw Error(`Expected vote signature of size 64, found something of size ${voteSignatureSize}`);
      }
      const voteSignature = script.slice(pos, pos + voteSignatureSize);
      pos += voteSignatureSize;
      const electionIDSize = script[pos++];
      if (electionIDSize !== 20) {
        throw Error(`Expected election ID of size 20, found something of size ${electionIDSize}`);
      }
      // skip ID
      pos += electionIDSize;

      const voteOptionHashLen = script[pos++];
      if (voteOptionHashLen !== 20) {
        throw Error('Expected election ID of size 20, found something of size $voteOptionHashLen');
      }
      const vote = Buffer.from(script.slice(pos, pos + voteOptionHashLen));
      assert(vote.length === 20);
      const valid = validHashedVoteOptions.find((x) => vote.compare(x));
      if (valid === undefined) {
        throw Error('Invalid vote option in transaction');
      }
      return [vote, voteSignature];
    }
}
