import {
  encodeDataPush, OpcodesCommon, encodeCashAddress, Transaction, CashAddressType,
} from '@bitauth/libauth';
import { PUBLIC_KEY_COMPACT_SIZE, SCHNORR_SIGNATURE_SIZE } from './networkconstants';
import { hash160 } from './crypto';
import InvalidPayloadError from './error/invalidpayloaderror';
import { copyStackElementAt } from './script';

/**
 * A Bitcoin Cash contract is a designed to allow maximum arbitrary data
 * payload in the input spending from the contract
 *
 * This contract is described in:
 * https://docs.voter.cash/input-payload-contract.html
 */
export namespace PayloadContract {
    export const MAX_PUSH1_SIZE = 520;
    export const MAX_PUSH2_SIZE = 520;
    export const MAX_PUSH3_SIZE = 463;
    export const MAX_PAYLOAD_PER_INPUT_SIZE = MAX_PUSH1_SIZE + MAX_PUSH2_SIZE + MAX_PUSH3_SIZE;
    export const REDEEMSCRIPT_PUBKEY_POS = 7;
    export const REDEEMSCRIPT_PAYLOADHASH_POS = 46;
    export const PAYLOAD_HASH_SIZE = 20;

    /**
     * The redeemscript hashes the payload and public key, then verifies that the
     * hash matches the expected input. Then it verifies the transaction itself
     * (like a normal P2PKH tx would)
     */
    export function redeemScript(pubkey: Uint8Array, payloadHash: Uint8Array): Uint8Array {
      return Buffer.concat([
        // hash push3
        Buffer.alloc(1, OpcodesCommon.OP_HASH160),

        // hash push2
        Buffer.alloc(1, OpcodesCommon.OP_SWAP),
        Buffer.alloc(1, OpcodesCommon.OP_HASH160),

        // concatenate hash of push3 and push2
        Buffer.alloc(1, OpcodesCommon.OP_CAT),

        // hash push1
        Buffer.alloc(1, OpcodesCommon.OP_SWAP),
        Buffer.alloc(1, OpcodesCommon.OP_HASH160),

        // concatenate hash1 to (push3 + push2)
        Buffer.alloc(1, OpcodesCommon.OP_CAT),

        // introduce the pubkey that will be required to spend the payload.
        // (the constant REDEEMSCRIPT_PUBKEY_POS should point to this push)
        encodeDataPush(pubkey),
        // take extra copy of the pubkey for checksig later
        Buffer.alloc(1, OpcodesCommon.OP_DUP),

        // hash pubkey and concat to (push3 + push2 + push1 ..)
        Buffer.alloc(1, OpcodesCommon.OP_HASH160),
        // after ROT, the stack is (signature, public key, push hashes, public key hash)
        Buffer.alloc(1, OpcodesCommon.OP_ROT),
        Buffer.alloc(1, OpcodesCommon.OP_CAT),

        // finally, the hash of hashes
        Buffer.alloc(1, OpcodesCommon.OP_HASH160),

        // compare it to he expected hash
        encodeDataPush(payloadHash),
        Buffer.alloc(1, OpcodesCommon.OP_EQUALVERIFY),

        // check signature
        Buffer.alloc(1, OpcodesCommon.OP_CHECKSIGVERIFY),

        // verify that there are no additional inputs
        Buffer.alloc(1, OpcodesCommon.OP_DEPTH),
        Buffer.alloc(1, OpcodesCommon.OP_NOT),
      ]);
    }

        /**
         * Transform payload into items that fit on the Bitcoin Script stack.
         */
        export function payloadToStackItems(payload: Uint8Array): Uint8Array[] {
          if (payload.length > MAX_PAYLOAD_PER_INPUT_SIZE) {
            throw Error(`Too large payload (${payload.length} > ${MAX_PAYLOAD_PER_INPUT_SIZE}`);
          }

          if (payload.length === 0) {
            throw Error('Payload cannot be empty');
          }

          const PUSH2_OFFSET = MAX_PUSH1_SIZE;
          const PUSH3_OFFSET = MAX_PUSH1_SIZE + MAX_PUSH2_SIZE;

          const push1 = payload.slice(0, Math.min(MAX_PUSH1_SIZE, payload.length));
          let push2 = new Uint8Array();
          let push3 = new Uint8Array();

          if (payload.length > PUSH2_OFFSET) {
            let toIndex = PUSH3_OFFSET;
            if (toIndex > payload.length) {
              toIndex = payload.length;
            }
            push2 = payload.slice(PUSH2_OFFSET, toIndex);
          }

          if (payload.length > PUSH3_OFFSET) {
            let toIndex = PUSH3_OFFSET + MAX_PUSH3_SIZE;
            if (toIndex > payload.length) {
              toIndex = payload.length;
            }
            push3 = payload.slice(PUSH3_OFFSET, toIndex);
          }

          return [push1, push2, push3];
        }

        export async function calculatePayloadHash(
          pubkey: Uint8Array,
          payload: Uint8Array,
        ): Promise<Uint8Array> {
          if (pubkey.length !== PUBLIC_KEY_COMPACT_SIZE) {
            throw Error(
              `Expected public key to be ${PUBLIC_KEY_COMPACT_SIZE} bytes got ${pubkey.length}`,
            );
          }

          const [push1, push2, push3] = payloadToStackItems(payload);

          const [push1h, push2h, push3h, pkh] = await Promise.all([
            hash160(push1),
            hash160(push2),
            hash160(push3),
            hash160(pubkey),
          ]);

          const concatenated = Buffer.concat([pkh, push3h, push2h, push1h]);

          return hash160(concatenated);
        }

    export async function unlockingScript(
      signature: Uint8Array,
      pubkey: Uint8Array,
      payload: Uint8Array,
    ): Promise<Uint8Array> {
      if (signature.length !== SCHNORR_SIGNATURE_SIZE) {
        throw Error(
          `Expected (Schnorr) signature to be ${SCHNORR_SIGNATURE_SIZE}, `
                    + `got ${signature.length}`,
        );
      }

      const [push1, push2, push3] = await payloadToStackItems(payload);
      const payloadHash = await calculatePayloadHash(pubkey, payload);
      const script = redeemScript(pubkey, payloadHash);

      return Buffer.concat([
        encodeDataPush(signature),
        encodeDataPush(push1),
        encodeDataPush(push2),
        encodeDataPush(push3),
        encodeDataPush(script),
      ]);
    }

        export function lockingScript(pubkey: Uint8Array, payloadHash: Uint8Array): Uint8Array {
          return Buffer.concat([
            Buffer.alloc(1, OpcodesCommon.OP_HASH160),
            encodeDataPush(payloadHash),
            Buffer.alloc(1, OpcodesCommon.OP_EQUAL)]);
        }

        export async function contractAddress(
          pubkey: Uint8Array,
          payloadHash: Uint8Array,
        )
                : Promise<string> {
          const hash = await hash160(redeemScript(pubkey, payloadHash));
          return encodeCashAddress('bitcoincash', CashAddressType.P2SH, hash);
        }

        /**
         * Validates that:
         * - Script is in fact this contract
         * - Payload hash matches payload and pubkey
         * - (NYI: Signature is valid for transaction)
         *
         * Throws on validation error
         *
         * @param script - The redeemScript.
         * @param payload - The payload this redeemScript is supposed to provide.
         */
        export async function validateRedeemScript(
          script: Uint8Array,
          payload: Uint8Array,
        ): Promise<void> {
          // Read the pubkey
          const pubkey = script.slice(
            REDEEMSCRIPT_PUBKEY_POS + 1,
            REDEEMSCRIPT_PUBKEY_POS + 1 + PUBLIC_KEY_COMPACT_SIZE,
          );

          // Read the payload cash
          const payloadHash = script.slice(
            REDEEMSCRIPT_PAYLOADHASH_POS + 1,
            REDEEMSCRIPT_PAYLOADHASH_POS + 1 + PAYLOAD_HASH_SIZE,
          );

          // Verify that it's the exact redeem script we expect
          const expectedScript = redeemScript(pubkey, payloadHash);

          if (!Buffer.from(script).equals(Buffer.from(expectedScript))) {
            throw new InvalidPayloadError('Incorrect redeemscript for payload contract');
          }

          // Verify for correct payload hash
          const h = Buffer.from(await calculatePayloadHash(pubkey, payload));
          if (!h.equals(payloadHash)) {
            throw new InvalidPayloadError('Payload Hash mismatch');
          }
          // TODO: Verify transaction signature. We have pubkey, signature and tx available.
        }

        /**
         * Read a payload from a transaction input.
         */
        export async function getPayloadFromInputScript(
          script: Uint8Array,
          validate: boolean,
        ): Promise<Uint8Array> {
          /* eslint-disable no-unused-vars, @typescript-eslint/no-unused-vars */
          const getStackItemAt = (offset: number, what: string) => {
            try {
              return copyStackElementAt(script, offset);
            } catch (e) {
              throw new InvalidPayloadError(`Error getting ${what}: ${e}`);
            }
          };

          // First 65 bytes should be signature.
          const [signature, _signatureStart, signatureEnd] = getStackItemAt(0, 'signature');
          if (signature.length !== SCHNORR_SIGNATURE_SIZE) {
            throw new InvalidPayloadError(
              `Expected signature of size ${SCHNORR_SIGNATURE_SIZE}, `
                    + `but got stack item of size ${signature.length}"`,
            );
          }
          // Payload Pushes
          const [push1, _push1Start, push1End] = getStackItemAt(signatureEnd + 1, 'push1');
          const [push2, _push2Start, push2End] = getStackItemAt(push1End + 1, 'push2');
          const [push3, _push3Start, push3End] = getStackItemAt(push2End + 1, 'push3');

          const payload = Buffer.concat([push1, push2, push3]);

          if (validate) {
            /* eslint-disable-next-line @typescript-eslint/no-shadow */
            const redeemScript = getStackItemAt(push3End + 1, 'redeemScript')[0];
            if (!redeemScript) {
              throw new InvalidPayloadError('No redeem script');
            }
            await validateRedeemScript(redeemScript, payload);
          }
          return payload;
        }

        /**
         * Read payload from all inputs
         *
         * @param tx - Transaction with payload
         * @param validate - Validate that it's a valid payload contract (slower)
         */
        export async function getPayloadFromTx(
          tx: Transaction,
          validate: boolean = true,
        ): Promise<Uint8Array> {
          if (tx.inputs.length === 0) {
            throw Error('Transaction has no inputs');
          }

          // We require the first input to contain a payload, so we want it
          // to throw on error (no try/catch)
          let payload: Buffer = Buffer.from(await getPayloadFromInputScript(
            tx.inputs[0].unlockingBytecode,
            validate,
          ));

          // Using more inputs to deliver a payload is optional.
          for (let i = 1; i < tx.inputs.length; ++i) {
            try {
              const inputPayload = await getPayloadFromInputScript(
                tx.inputs[i].unlockingBytecode,
                validate,
              );
              payload = Buffer.concat([payload, inputPayload]);
            } catch (e) {
              // Ignoring inputs and all subsequent inputs.
              // TODO: Ignore only exceptions related to parsing payload from input script.
              return payload;
            }
          }
          return payload;
        }

    }
